<?php include 'conn.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style type="text/css">
		td{
		vertical-align: middle !important;
		padding: 3px 6px;
	}
	#supplier thead{
		font-weight: bold;
		font-size: 16px;
		padding: 0px 2px;

	}
	#supplier{
		font-family: Times New Romans;
		border-collapse: collapse;
		width: 80%;
	}
	#supplier td, #supplier th {
		border: 1px solid #ddd;
		padding: 3px;
}
#supplier tr:nth-child(even){background-color: #f2f2f2;}

#supplier tr:hover {background-color: #ddd;}

#supplier th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
  font-size: 20px;
}
	h4{
		text-align: center;
		font-style: times new romans;
		font-size: 20px;
	}

	</style>
</head>
<body>
	<table id="supplier">
		<h4>List of Customers</h4>
		<thead>
			<tr>
				<th class="text-center">#</th>
				<th class="">Account Number</th>
				<th class="">Supplier Name</th>
				<th class="">Email Address</th>
				<th class="">Contacts</th>
				<th class="">KRA Pin*</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$i = 1;
			$query = "SELECT acc_no, name, email, phone, pin FROM customer ORDER BY name asc";
			$result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
			$numrows = pg_num_rows($result);
			
			?>
			<?php
			for($ri = 0; $ri < $numrows; $ri++) {
				echo "<tr>\n";
				$row = pg_fetch_assoc($result, $ri);
				echo "<td>", $i++, "</td>
				<td>", $row['acc_no'], "</td>
				<td>",$row['name'], "</td>
				<td>", $row['email'], "</td>
				<td>", $row['phone'], "</td>
				<td>", $row['pin'], "</td>
			</tr>";
		}
		pg_close($conn);
		?>
		</tbody>
	</table>
</body>
</html>